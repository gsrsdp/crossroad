"""Common scripts, classes and functions

Repository: https://gitlab.com/gsrsdp/crossroad

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import os
import socket
import traceback
from datetime import datetime
import smtplib
from email.message import EmailMessage
from PySide2 import QtWidgets, QtCore

import Metashape
from common.startup.initialization import config
import base64
import json
from pathlib import Path

REPORT_FLAG = config.get('Options', 'report_about_errors') == 'True'


class LoggerError(Exception):
    pass


class LoggerValues:
    def __init__(self, input: dict, plugin_name: str, plugin_version=None, reply_email=None, user_annotation=None):
        """
        Helper to log errors which produced by Metashape plugins.
        :param input: dict. Input data of your app.
        :param plugin_name: App name.
        :param plugin_version: App version.
        :param reply_email: Add user email to message.
        :param user_annotation: Text message by user.
        """

        self.input = input
        self.plugin_name = plugin_name
        self.plugin_version = str(plugin_version)
        self.plugins_build = self.get_plugins_version()
        self.reply_email = str(reply_email)
        self.user_annotation = user_annotation
        self.metashape_version = str(Metashape.version)

    @property
    def text_input(self):
        return "\n".join(["{}: {}".format(k, v) for k, v in self.input.items()])

    @staticmethod
    def get_plugins_version():
        from version import __version__
        return __version__


def build_error_message(values: LoggerValues, error_type, error_text):
    """
    Build string subject and text for email message.
    :param values:
    :param error_type:
    :param error_text:
    :return:
    """

    timestamp = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    subject = "{}: {}".format(values.plugin_name, error_type)
    user_ann = "" if values.user_annotation == '' else "User annotation:\n\n{}\n\n".format(values.user_annotation)

    reply = "Reply email: {}\n\n".format(values.reply_email)
    text = f"Date: {timestamp}\n\n" \
           f"Plugin name: {values.plugin_name}\n" \
           f"Plugin version: {values.plugin_version}\n" \
           f"Plugins build: {values.plugins_build}\n" \
           f"Metashape version: {values.metashape_version}\n\n" \
           f"Input values:\n\n{values.text_input}\n\n" \
           f"{user_ann}" \
           f"Error:\n\n{error_text}"

    return subject, (reply if values.reply_email else '') + text


def send_email(server_params, user_to, subject, text):
    try:
        server = smtplib.SMTP_SSL(server_params['host'], server_params['port'])
        server.login(server_params['app_email'], server_params['password'])

        msg = EmailMessage()
        msg['Subject'] = subject
        msg['From'] = server_params['app_email']
        msg['To'] = user_to
        msg.set_content(text)

        server.send_message(msg)
        server.quit()
    except socket.gaierror:
        raise Warning("Sending error log to developers is failed.")


def send_geoscan_plugins_error_to_devs(error, values):
    logger_params = get_logger_params()
    server_params = logger_params['init']
    developers_mail = logger_params['developers']
    error_type = error.strip().split("\n")[-1].split(':')[0]
    subject, text = build_error_message(values=values, error_type=error_type, error_text=error)

    try:
        send_email(server_params=server_params,
                   user_to=developers_mail,
                   subject=subject,
                   text=text)
    except:
        print("[plugins] during send errors to devs another error occurred (usually 'no network')")


def show_message_error(text):
    msg = QtWidgets.QMessageBox()
    msg.setWindowTitle('Error')
    msg.setIcon(QtWidgets.QMessageBox.Critical)
    msg.setText(text)
    msg.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
    msg.exec_()


def log_method(plugin_name: str, version: str):
    """To use this method self.log_values -> dict method is required in class object."""

    def decorator(func):
        def wrapper(self, *args, **kwargs):
            try:
                func(self, *args, **kwargs)
            except Exception:
                values = LoggerValues(input=self.log_values(), plugin_name=plugin_name, plugin_version=version)
                send_geoscan_plugins_error_to_devs(error=traceback.format_exc(), values=values)
                show_message_error(text=traceback.format_exc())
        return wrapper
    return decorator


def log_func(plugin_name: str, version: str, input_items):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
                return res
            except Exception:
                values = LoggerValues(input=input_items, plugin_name=plugin_name, plugin_version=version)
                send_geoscan_plugins_error_to_devs(error=traceback.format_exc(), values=values)
                show_message_error(text=traceback.format_exc())
        return wrapper
    return decorator


def log_method_by_crash_reporter(plugin_name: str, version: str):
    """To use this method self.log_values -> dict method is required in class object."""

    import common.loggers.crash_reporter as cr

    def decorator_class(func):
        def wrapper(self, *args, **kwargs):
            try:
                res = func(self, *args, **kwargs)
                return res
            except Exception:
                if REPORT_FLAG:
                    values = LoggerValues(input=self.log_values(), plugin_name=plugin_name, plugin_version=version)
                    cr.run_crash_reporter(error=traceback.format_exc(), values=values, run_thread=False)
                else:
                    show_message_error(text=traceback.format_exc())
        return wrapper
    return decorator_class


def log_func_by_crash_reporter(plugin_name: str, version: str, items: dict):
    import common.loggers.crash_reporter as cr

    def decorator_func(func):
        def wrapper(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
                return res
            except Exception:
                if REPORT_FLAG:
                    values = LoggerValues(input=items, plugin_name=plugin_name, plugin_version=version)
                    cr.run_crash_reporter(error=traceback.format_exc(), values=values, run_thread=False)
                else:
                    show_message_error(text=traceback.format_exc())
        return wrapper
    return decorator_func


def __testcase():
    input_items = {"Rover": "asda.txt", "Base": "base.txt", 'unit': 2}

    @log_func(plugin_name='test_app', version='0.4.7', input_items=input_items)
    def testcase1():
        shit_happens_value = 1 / 0

    testcase1()


def get_logger_params():
    with open(Path(os.path.dirname(os.path.abspath(__file__))) / 'logger_params.bin', 'rb') as file:
        encoded_f = file.read()
        decoded_f = base64.b64decode(encoded_f)
    return json.loads(decoded_f.decode('utf8').replace("'", '"'))


if __name__ == "__main__":
    __testcase()