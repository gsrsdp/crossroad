��            )   �      �     �     �     �  
   �     �  !   �               8     =     V     i     y     �  &   �  1   �  *   �  G     6   Z     �     �     �     �  $   �  E   �     =     [     p     �    �  ,   �      �     �          9  5   Q     �  S   �     �  /   �      %  ,   F     s     �  S   �  h   �  N   Q	  �   �	  z   *
     �
  #   �
  ;   �
  &     W   ;  �   �  X     '   o  :   �  (   �                         	                                          
                                                                      Aborted by user! Agisoft Metashape plugins Chose directory Chose file Color error Do you want to update {} plugins? Error Get auto updates for Plugins Help Installing site-packages No camera selected Not a directory Not valid color:  Ok Path:
{}
Is not an existing directory! Please open document which you want to work with! Please select camera you want to work with Please, close all other Agisoft Metashape applications or click Cancel. Please, re-run Agisoft Metashape to apply new settings Plugins Plugins Settings Processing failed! Processing finished! Processing stopped! Aborted by user! Python packages update is required (Internet connection is required). Send error logs to developers Unexpected error!
{} Updating specific packages Updating {} plugins Project-Id-Version: 2.1.2.0
PO-Revision-Date: 2024-09-21 12:00+0300
Last-Translator: Gennadii Enov <gennadiienov@gmail.com>
Language-Team:
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 Прервано пользователем! Плагины Agisoft Metashape Выберите каталог Выберите файл Ошибка цвета Вы хотите обновить {} плагины? Ошибка Получать автоматические обновления плагинов Помощь Устанавливаем библиотеки Не выбрана камера Не является директорией Неверный цвет:  Ок Путь:
{}
Не является существующей директорией! Пожалуйста откройте документ, с которым хотите работать! Выберите камеру, с которой хотите работать Пожалуйста, закройте открытые проекты Agisoft Metashape, либо отмените обновление. Пожалуйста, перезапустите Agisoft Metashape для применения новых настроек Плагины Настройки плагинов Обработка завершилась неудачно! Обработка завершена! Обработка остановлена! Прервано пользователем! Необходимо обновление пакетов python (требуется подключение к Интернету). Отправлять анонимные логи ошибок разработчикам Неожиданная ошибка!
{} Обновление специальных пакетов Обновление {} плагинов 