"""Common scripts, classes and functions

Repository: https://gitlab.com/gsrsdp/crossroad

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import os

from common.startup.initialization import config


PLUGINS_DIR = config.get('Paths', 'scripts')


def create_requirements():
    packages = get_all_packages()
    requirements = os.path.join(PLUGINS_DIR, 'requirements.txt')
    with open(requirements, 'w') as file:
        data = list()
        for package, version in sorted(packages.items(), key=lambda item: item[0].lower()):
            if version:
                data.append("{}=={}\n".format(package, version))
            else:
                data.append("{}\n".format(package))
        file.writelines(data)


def get_all_packages():
    packages = dict()
    for item in os.listdir(PLUGINS_DIR):
        if item == 'local_repo':
            continue
        obj = os.path.join(PLUGINS_DIR, item)
        if os.path.isdir(obj) and not item.startswith('.'):
            for file in filter(lambda x: x == 'requirements.txt', os.listdir(obj)):
                parse_requirements_txt(source=os.path.join(obj, file), packages=packages)
    return packages


def parse_requirements_txt(source: str, packages: dict):
    with open(source, 'r') as file:
        for line in file.readlines():
            data = [x.strip() for x in line.split('==')]
            package = data[0] if data[0] not in ['', '\n'] else None
            version = data[1] if len(data) == 2 else None

            if package and package not in packages:
                packages[package] = version

