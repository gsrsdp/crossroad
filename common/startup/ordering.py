from pathlib import Path
import re


def get_menu_items(init_plugin):
    """
    Returns a list of menu items, which contains this plugin
    :param init_plugin: a path to plugin init file (scripts/installed_plugins)
    """
    menu_items = []

    with open(init_plugin, 'r') as f:
        items = re.findall(r'_\(TOP_MENU\) \+ "/" \+ _\("[\w\s]*"\)', f.read())

        for item in items:
            menu_items.append(item.split('_("')[1][:-2])    # remove unnecessary symbols in MENU_ITEM") string

    return list(set(menu_items))


def sort_menu_items(scripts_path):
    """
    Rewrite a file plugins.txt for alphabetic menu initialization
    :param scripts_path: a path to scripts folder
    """
    res = {}
    plugins_dir = Path(scripts_path) / 'installed_plugins'

    for file in plugins_dir.glob('*.py'):
        if file.name != '__init__.py':
            try:
                menu_items = get_menu_items(file)
                menu_items.sort()
                item = menu_items[-1]   # if one init-file initialize a few plugins in different menu items, take last
                if item not in res:
                    res[item] = [file.name[:-3]]    # remove '.py'
                else:
                    res[item].append(file.name[:-3])
            except:
                pass

    res = dict(sorted(res.items()))     # alphabetic

    if res:
        if 'Other' in res:
            res['Other'] = res.pop('Other')  # move in the tail of dict
        if 'Settings' in res:
            res['Settings'] = res.pop('Settings')

        with open(Path(scripts_path) / 'installed_plugins' / 'plugins.txt', 'w') as f:
            for menu_item in res:
                for plugin in res[menu_item]:
                    f.write(f'{plugin}\n')  # rewrite plugins.txt
