"""Utilities to load plugins to Agisoft Metashape

Repository: https://gitlab.com/gsrsdp/crossroad

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""


def init_top_menu():
    """Name of plugins head menu"""
    top_menu_name = _('Plugins')
    return top_menu_name
