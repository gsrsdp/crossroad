# Плагины для Agisoft Metashape

Данные плагины предназначены для расширения функциональных возможностей [Agisoft Metashape](https://www.agisoft.com/). Плагины представляют собой отдельные программы с собственным интерфейсом, которые запускаются внутри Agisoft Metashape.

Узнать подробнее о возможностях каждого из плагинов можно ниже, перейдя по ссылке с интересующим плагином.

Если у вас возникли проблемы по работе с плагинами, либо появились замечания и предложения, то мы будем рады, если вы оставите свой комментарий в телеграм [группе](https://t.me/metashape_plugins).

## Содержание
- [Системные требования](#системные-требования)
- [Лицензия](#лицензия)
- [Установка](#установка)
- [Список плагинов](#список-плагинов)
- [Автоматическое обновление](#автоматическое-обновление)
- [Сбор информации об ошибках](#сбор-информации-об-ошибках)
- [Удаление](#удаление)
- [Примечание](#примечание)

## Системные требования
- ОС **Windows**
- **Agisoft Metashape** версии **1.8** и выше. 

## Лицензия
Данное программное обеспечение имеет лицензию GNU General Public License version 3.
Подробнее - в файле [LICENSE](https://gitlab.com/gsrsdp/crossroad/-/blob/master/LICENSE).

## Установка
Для установки вы можете воспользоваться одним из способов:

***Способ 1***. [Скачать](https://gitlab.com/gsrsdp/crossroad/-/raw/Installer/Plugins_lite.exe?inline=false) установщик плагинов и установить. 

***Способ 2***. Скачать исходный код из репозитория и разместить в папке _%localappdata%\Agisoft\Metashape Pro\scripts_ (если папки scripts не существует, то необходимо ее создать). 

_После установки при первом запуске Agisoft Metashape требуется иметь подключение к сети Интернет для установки необходимых зависимостей._

## Список плагинов
### Блок **ГНСС**
* [Обработка геодезических измерений](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/gnss_post_processing#readme)
* [Соединить Magnet XML файлы](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/magnet_merger#readme)
### Блок **Камеры**
* [Коррекция гаммы изображений](https://gitlab.com/gsrsdp/crossroad/-/tree/dev/auto_gamma_correction#readme)
* [Вертикальное выравнивание](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/fast_layout#readme)
* [Оценка качества исходных снимков](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/quality_estimator#readme)
* [Микшер каналов изображений](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/image_channel_mixer#readme)
* [Построить маски по контурам](https://gitlab.com/gsrsdp/crossroad/-/tree/dev/make_masks_from_shapes#построить-маски-по-контурам)
* [Спрятать объект в контуре](https://gitlab.com/gsrsdp/crossroad/-/tree/dev/make_masks_from_shapes#спрятать-объект-в-контуре)
* [Удалить дубликаты изображений](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/remove_image_duplicates#readme)
### Блок **Фигуры**
* [Построить буферную зону](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/buffer_by_markers#readme)
* [Экспортировать по фигурам](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/export_by_shapes#readme)
* [Экспорт плотного облака по фигурам](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/export_dense_cloud_by_shapes/README.md)
* [Построить номенклатурную разграфку](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/shape_worker#построить-номенклатурную-разграфку)
* [Задать высоту выбранной фигуре](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/set_altitudes_for_shape#readme)
* [Построить регулярную сетку](https://gitlab.com/gsrsdp/crossroad/-/tree/dev/shape_worker#построить-регулярную-сетку)
* [Работа с фигурами](https://gitlab.com/gsrsdp/crossroad/-/tree/dev/make_masks_from_shapes#работа-с-фигурами)
### Блок **Модель**
* [Экспорт/Импорт модели по маркеру](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/expimp_by_marker#readme)
* [Построить стены](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/mesh_creator#readme)
### Блок **Другое**
* [Задать регион](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/chunk_region_setter#readme)
* [Добавить пользовательские системы координат](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/crs_uploader#readme)
* [Создание MapInfo TAB-файлов для ортофотопланов](https://gitlab.com/gsrsdp/crossroad/-/blob/dev/tab_meta_creator#readme)

## Автоматическое обновление
Исходный код плагинов может быть автоматически обновлен на компьютерах пользователей. 
Автоматические обновления могут быть отключены в настройках плагинов.

## Сбор информации об ошибках
По умолчанию, в плагинах включена опция сбора логов о некоторых неожиданных ошибках, которые отправляются разработчикам. 
Сбор логов может быть отключен в настройках.

## Удаление

Для полного удаления плагинов необходимо удалить следующие папки:
- _%localappdata%\Agisoft\Metashape Pro\scripts_
- _%localappdata%\Agisoft\Metashape Pro\site-packages-py3[n]_
- _%localappdata%\Agisoft\Metashape Pro\resources_

## Примечание

Изначально, данные плагины разрабатывались программистами и ГИС-специалистами компании Геоскан для автоматизации и решения своих ежедневных задач, связанных с обработкой аэрофотосъемочных данных. 

Мы выкладываем исходный код данных программ в состоянии **как есть** и надеемся, что данные плагины и их исходный код помогут вам в решении своих собственных задач при помощи Agisoft Metashape.
